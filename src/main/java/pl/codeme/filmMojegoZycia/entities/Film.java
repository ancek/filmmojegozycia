package pl.codeme.filmMojegoZycia.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Film  {
	
	@Id
	private Integer id;
	private String title;
	private String year;
	private boolean seen;
	private Integer rate;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public boolean isSeen() {
		return seen;
	}
	public void setSeen(boolean seen) {
		this.seen = seen;
	}
	public Integer getRate() {
		return rate;
	}
	public void setRate(Integer rate) {
		this.rate = rate;
	}
	
	public String toString(){
		return "Id: " + getId() + " Title: " + getTitle() + " Year: " + getYear();
	}

}
