package pl.codeme.filmMojegoZycia;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Hello world!
 *
 */
public class App {
	
	public static EntityManagerFactory factory;
	public static EntityManager em;
	
    public static void main( String[] args ){
    	
		factory = Persistence.createEntityManagerFactory("codeme2");
		em = factory.createEntityManager();
		
    }
}
